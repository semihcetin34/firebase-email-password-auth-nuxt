import colors from 'vuetify/es5/util/colors'

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - nuxt-firebase-auth',
    title: 'Elegant Avize',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {rel: 'stylesheet', href: '/css/bootstrap.min.css'},
      { href: 'https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&display=swap" rel="stylesheet'},
      {rel: 'stylesheet', href: '/css/boxicon.min.css'},
      {rel: 'stylesheet', href: '/css/custom.css'},
      {rel: 'stylesheet', href: '/css/templatemo.css'}
    ],
    script:[
      {src:'/js/bootstrap.bundle.min.js'},
      {src:'/js/jquery.min.js'},
      {src:'/js/isotope.pkgd.js'},
      {src:'/js/templatemo.js'},
      {src:'/js/custom.js'}
    ],
    htmlAttrs:{
      Lang:"tr"
    }
  },
  body:{

  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],
  router: {
    middleware: ['auth']
  },
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',

      [
        '@nuxtjs/firebase',
        {
          config: {
             /* apiKey: "AIzaSyBIpGTddqJVdxjvvS7BrURpcGDpP3zypJE",
              authDomain: "fir-nuxt-auth-12f4a.firebaseapp.com",
              projectId: "fir-nuxt-auth-12f4a",
              storageBucket: "fir-nuxt-auth-12f4a.appspot.com",
              messagingSenderId: "7425042073",
              appId: "1:7425042073:web:94dc7a2bf077ff8e9c3a08"*/
            apiKey: "AIzaSyCFdXZkuP835w6UitpL5VoXBje5X0Re9uM",
            authDomain: "elegant-sakarya.firebaseapp.com",
            projectId: "elegant-sakarya",
            storageBucket: "elegant-sakarya.appspot.com",
            messagingSenderId: "980005397914",
            appId: "1:980005397914:web:f9d862e30c8dc3fc194623",
            measurementId: "G-H11EXXBVZ9"
          },
          services: {
            auth: {
              persistence: 'local', // default
              initialize: {
                onAuthStateChangedAction: 'onAuthStateChangedAction',
                subscribeManually: false
              },
              ssr: false,
            }
          }
        }
      ]
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'tr'
    }
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
